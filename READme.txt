FANTASY FIGHTING GAME!!!!

Overview:
This game will consist of a player being able to fight in an arena type game, moving from opponent to opponent to a boss, where they can buy better items aswell as gain experience.  After defeating an enemy,  the player can buy health potions(healht pots) to get health back in the game. The game will also have a shop where the player can buy items and sell items with their coins to gain better stats and defeat better opponents. The player will gain coins after killing an oppenent.  Coins can be spent at the store.  After battling an enemy, the player will have an option to continue fighting, go to the store, save the game, or exit.  When the player is fighting an enemy, the player will take turns with the enemy.  During each turn the player can decide to attack, heal, or run away.  If the player chooses to run, the enemy's health will be restored.  

Main menu:
In the main menu, the player will have the option to load game or create new game.  There will be only one save file so if the player decides to load a game, the game saved in that file will be loaded.  If the player decides to start a new game and save it, then the file will be overwritten. 
If the player chooses a new game, the player will pick a name and will have the option to buy a few items.  After these selections have been made then the game begins.  

Store:
The store will have preset items that the user can buy or sell for predetermined prices. A player can open the store, add or remove items from a cart, and checkout if they have enough coins.  Upon checkout, the stats of the player will be updated accoring to their new inventory.

Inventory:
The players inventory will be able to add items aswell as remove them when buying or selling items.  The player will also have the option of viewing all items in their inventory.  

Items:
Items will come in four main categories: Weapon, Armor, Sheild, and Gloves.  All have different abilities adding to the character's stats. The armour adds armor which will reduce incoming damage.  Weapon will add to outgoing damage. Sheild will add to the players life.  The gloves add a coin bonus that increase the amount of coins the player gets within the arena.  

Hero:
Heros will be playable by the user.  The Hero is able to buy and sell items with the coins they collect by fighting villians in the arena.  They can buy these things at the store.  When an item is bought, the player's stats will be updated accordingly.  Same when selling an item. The player will have an option to view their own stats in game aswell. During battle the player can run, fight, or use a health pot.  If the player diesm then the game is over and the player can start again.  After defeating a villian, the player can go to the store, fight another monster, view stats, view inventory, save the game, or exit the game.  

Villians:
There will be various villians in the arena all set to starting statistics.  The player will advance their way through to try and defeat the boss.  

Data Files:
The data files requires to run this game include:
items.txt
characters.txt
save_file.txt

Class Hierarchies:
Salable: 
Has-a  |  Is-a
N/a       N/a

Items (Armor, shield, weapon, gloves, health pot):
Has-a  |  Is-a
N/a       Salable

BaseStorage:
Has-a  |  Is-a
N/a       N/a

Store:
Has-a  |  Is-a
Salable   BaseStorage

Inventory:
Has-a  |  Is-a
Salable   BaseStorage

BasePlayer:
Has-a  |  Is-a
N/a       N/a

BaseHero:
Has-a  |  Is-a
inventory    BasePlayer

BaseVillian:
Has-a  |  Is-a
N/a       BasePlayer