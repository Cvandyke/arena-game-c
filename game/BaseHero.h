//
// Created by Chandler on 11/27/2016.
//

#ifndef LETSFIGHT_BASEHERO_H
#define LETSFIGHT_BASEHERO_H

#include "BasePerson.h"
#include "Inventory.h"
#include "fstream"


class BaseHero : public BasePerson
{
protected:
    int coinBonus;
    Inventory purse;

public:
    /**
     * set coin bonus = 0
     * call deafult constructor on basePerson
     * @return
     */
    BaseHero();

    /**
     * set name, coins, health, armor, health, armor, damage, coinBonus
     * also using BasePerson Constructor
     * @param name
     * @param coins
     * @param health
     * @param armor
     * @param damage
     * @param coinBonus
     * @return
     */
    BaseHero(string name, int coins, int health, int armor, int damage, int coinBonus);

    /**
     * @return coin bonus
     */
    int getCoinBonus();

    /**
     * set CoinBonus
     * @param coinBonus
     */
    void addCoinBonus(int coinBonus);

    /**
     * remove coinbinus from CoinBonus
     * @param coinBonus
     */
    void removeCoinBonus(int coinBonus);


    /**
     * call sellItem on inventory
     * if not health pot, remove characteristic from player stats
     * @return item
     */
    Salable* sellItemFromPurse();

    /**
     * itereate through items and add to inventory
     * @param items
     */
    void addItemsToPurse(vector<Salable*> items);

    /**
     * call purse add coins
     * @param coinsToAdd
     */
    void addCoinsToPurse(int coinsToAdd);

    /**
     * call purse remove coins
     * @param coinsToRemove
     */
    void removeCoinsFromPurse(int coinsToRemove);

    /**
     * get all health pots in inventory
     * ask user which to use
     * use health pot
     */
    void useHealthPot();

    /**
     * show storage
     */
    void showInventory();

    /**
     * print stats in following format"
     * Name:  ....
     * Coins: ....
     * Number of Items: ...
     * Health: ....
     * Damage: ....
     * Armor: ....
     * Coin Bonus: ...
     */
    void showPlayerStats();

    /**
     * return number of coins in inventory
     */
    int getCoins();

    /**
     * call saveItems() on purse
     */
    void savePurse();

    /**
     * adjust stats and add to purse
     * @param item
     */
    void addItem(Salable* item);

    /**
     * delete all items in purse
     */
    ~BaseHero();

};


#endif //LETSFIGHT_BASEHERO_H
