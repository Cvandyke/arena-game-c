//
// Created by Chandler on 11/30/2016.
//

#include "BaseVillian.h"

BaseVillian::BaseVillian() {
    //initiate name, health, armor, and damge to default null

}

BaseVillian::BaseVillian(string name, int health, int armor, int damage, int coins) : BasePerson(name, health, armor, damage) {
    //initiate name, health, armor, and damage in BasePerson special constructor
    this->coins = coins;
    this->fullHealth = health;
}

void BaseVillian::heal() {
    //return health to fullHealth
    this-> health = fullHealth;
    return;
}

int BaseVillian::getCoins() {
    return coins;
}
