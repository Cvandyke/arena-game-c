//
// Created by Chandler on 11/1/2016.
//

#ifndef LETSFIGHT_GLOVE_H
#define LETSFIGHT_GLOVE_H

#include "Salable.h"

class Glove : public Salable {
protected:
    int coinBonus;


public:
    /**
    * defualt constructor
    * @return
    */
    Glove();

    /**
    * set type, name, ability, armor, buyprice
    * @param name
    * @param coinbonus
    * @param buyPrice
    * @return
    */
    Glove(string name, int coinBonus, int buyPrice);

    /**
     * get coin bonus
     * @return coinBonus
     */
    int getAbilityPower();

    /**
     * name to null
     * coin bonus to 0
     * buyprice to 0
     * sell price to 0
     */
    ~Glove();
};
#endif //LETSFIGHT_GLOVE_H
