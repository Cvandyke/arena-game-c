//
// Created by Chandler on 11/1/2016.
//

#ifndef LETSFIGHT_STORE_H
#define LETSFIGHT_STORE_H

#include "BaseStorage.h"
#include "Salable.h"
#include "fstream"


class Store: public BaseStorage
{
private:
    vector<Salable*> shop;

public:

    Store();

    /**
     * open file
     * iterate through file to create items
     * close file
     */
    void createItems();

    /**
    * print out a list of the stores items
    * format: "Number Type   Name  Ability   Ability power  buy price  sell price"
    * iterate through items in storage and print
    */
    void showStore();

    /**
     * ask int of item to add
     * check input
     *  check if int
     *  check if in range
     * get item from shop
     * add item to storage
     * add item buy price to total coins
     */
    void addToCart();

    /**
     * ask fro int of item to remove
     * check input
     *  check if int
     *  check if out of range
     * remove item from storage
     * remove item buy price from total coins
     */
    void removeFromCart();

    /**
     * set total coins = 0
     */
    void resetCoins();

    /**
     * empty cart
     */
    void emptyCart();

    /**
     * take item in and add it back into store
     */
    void itemReturn(Salable* item);

    /**
     * print "CART:"
     * call show storage
     */
    void showCart(int playerCoins);

    /**
     *
     * @return cart
     */
    vector<Salable*> getCart();

    /**
     * itereate through items in shop and add in order:
     * store(loaction)|type|name|AP|BP|
     * @param file
     */
    void saveItems();

    /**
     * delete everthing in shop
     * call delete storage
     */
    ~Store();

};


#endif //LETSFIGHT_STORE_H
