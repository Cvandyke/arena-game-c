//
// Created by Chandler on 11/26/2016.
//

#ifndef LETSFIGHT_BASEPERSON_H
#define LETSFIGHT_BASEPERSON_H

#include <string>
#include <iostream>

using namespace std;

class BasePerson
{
protected:
    string name;
    int health;
    int armor;
    int damage;

public:
    /**
     * set name, coins, health, armor, and damage to default 0
     * @return
     */
    BasePerson ();

    /**
     * set name coins health armor and damage to parameters
     * @param name
     * @param coins
     * @param health
     * @param armor
     * @param damage
     * @return
     */
    BasePerson(string name, int health, int armor, int damage);

    /**
     *
     * @return name
     */
    const string &getName() const;

    /**
     *
     * @return health
     */
    int getHealth() const;

    /**
     *
     * @return Armor
     */
    int getArmor() const;

    /**
     *
     * @return damage
     */
    int getDamage() const;

    /**
     * decrease health by incoming attackDamage
     * @param attackDamage
     * @return true if dead, false if not
     */
    bool getAttacked(int attackDamage);

    /**
     * add health to health
     * @param health
     */
    void addHealth(int health);

    /**
     * add armor to armor
     * @param armor
     */
    void addArmor(int armor);

    /**
     *add damage to damage
     * @param damage
     */
    void addDamage(int damage);

    /**
     * remove health from health
     * @param health
     */
    void removeHealth(int health);

    /**
     * remove armor from armor
     * @param armor
     */
    void removeArmor(int armor);

    /**
     * remove armor from armor
     * @param damage
     */
    void removeDamage(int damage);

};


#endif //LETSFIGHT_BASEPERSON_H
