//
// Created by Chandler on 12/4/2016.
//

#ifndef LETSFIGHT_ARENA_H
#define LETSFIGHT_ARENA_H

#include "BaseVillian.h"
#include "BaseHero.h"
#include <string>

class Arena {
private:
    int turnNumber;
    vector<BaseVillian*> enemies;
    int level = 0;
    string winner = "";

    /**
     * generate a random number
     * determine if odd or even
     * return 1 or 0
     */
    int getRandomNumber();

    /**
     * print player options:
     * 1: attack
     * 2: heal
     * 3: run
     */
    int playerOptions();

    /**
     * give enemy full health
     * exit arena
     */
    void run(BaseVillian* villian);

    /**
     * get damage from attPlayer
     * get armor from deffPlayer
     * find resulting attack damage
     * deffPlayer gets attacked
     * @param attPlayer
     * @param deffPlayer
     * @return true if deffPlayer is dead, else false
     */
    bool playerAttacking(BasePerson* attPlayer, BasePerson* deffPlayer);

public:
    /**
     * itereate through character.txt file to create items
     */
    void createEnemies();

    /**
     * call get random number for first player turn
     * if enemy:
     *      enemy attacks player
     * if player:
     *      display player options
     *      if 1 ==> player attacks enemy
     *      if 2 ==> player uses health pot
     *      if 3 ==> player runs from battle
     * continue until player runs or one is dead.
     * @param player
     * @return winner of fight ==> if player ran, winner is null
     */
    string fight(BaseHero* player);

    /**
     * set level to level set
     * @param levelSet
     */
    void setLevel(int levelSet);

    /**
     *
     * @return level
     */
    int getLevel();

    /**
     * figure out how many enemies are left in arena
     * @return
     */
    int enemiesLeft();

    /**
     * iterate through enemies to delete them
     */
    ~Arena();

};


#endif //LETSFIGHT_ARENA_H
