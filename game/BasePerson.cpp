//
// Created by Chandler on 11/26/2016.
//

#include "BasePerson.h"

BasePerson::BasePerson() {

}

BasePerson::BasePerson(string name, int health, int armor, int damage) {
    //set name
    this-> name = name;
    //set health
    this-> health = health;
    //set armor
    this-> armor = armor;
    //set damage
    this-> damage = damage;
}


const string &BasePerson::getName() const {
    return name;
}


int BasePerson::getHealth() const {
    return health;
}

int BasePerson::getArmor() const {
    return armor;
}

int BasePerson::getDamage() const {
    return damage;
}

bool BasePerson::getAttacked(int attackDamage) {
    //decrease health by incoming attackDamage
    health -= attackDamage;
    //check if dead
    bool dead = false;
    if (health <= 0)
    {
        dead = true;
        health = 0;
        cout << name << " is dead!" << endl;
    }
    //return true if dead, false if alive
    return dead;
}

void BasePerson::addHealth(int health) {
    this->health += health;
    return;
}

void BasePerson::addArmor(int armor) {
    BasePerson::armor += armor;
    return;
}

void BasePerson::addDamage(int damage) {
    BasePerson::damage += damage;
    return;
}

void BasePerson::removeHealth(int health) {
    this->health-=health;
    return;
}

void BasePerson::removeArmor(int armor) {
    this->armor-=armor;
    return;
}

void BasePerson::removeDamage(int damage) {
    this->damage-=damage;
}



