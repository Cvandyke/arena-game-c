//
// Created by Chandler on 12/4/2016.
//

#ifndef LETSFIGHT_GLADIATORGAME_H
#define LETSFIGHT_GLADIATORGAME_H

#include "BaseHero.h"
#include "Store.h"
#include "Arena.h"
#include <string>

class GladiatorGame {
private:
    BaseHero* player;
    Arena coliseum;
    Store store;
    string winner;

    /**
     * print instructions to game
     */
    void printInstructions();

    /**
     * find file that has saved data
     * open file
     * create cahracter
     * add items to store or to character inventory
     * set arena level to level from file n
     */
    bool loadGame();

    /**
     * get character name
     * create new character
     * create shop
     */
    void createGame();

    /**
     * using a while loop-- while not valid option
     * ask player if they want to:
     * 1: fight
     * 2: go shopping
     * 3: view charcter stats
     * 4: save game
     * 5: exit game
     * check if input is an int
     * check if in range
     * @return
     */
    int gameOptions();

    /**
     * ask if they want to buy or sell
     *   if buy
     *      show cart and shop
     *      options 1) add item to cart
     *          2) remove item from cart
     *          3) check out"
     *      get choice
     *          if choice 1- call addToCart
     *          if choice 2- call removeFromCart
     *          if choice 4- call checkout
     *      if checkout
     *          playercoins -= store totalcoins
     *          store.set total coins = 0
     *          add cart to player inventory
     *          empty cart
     *          leave store
     *   if sell
     *      call sellItemFromPurse on character, return item to store
     */
    void shop();

    /**
     * call arena fight with player
     * @return
     */
    string fight(BaseHero* player);

    /**
     * call showPlayerStats
     */
    void characterStats(BaseHero* player);

    /**
     * write to file save_game.txt
     * write charcter info
     * write info on each item in store and inventory
     */
    void save();

    /**
     * ask to load or create game
     * load or create game
     */
    void getGame();

    /**
     * print all closing information
     */
    void printCredits();

    /**
     * print 30 blank lines for spacing
     */
    void spacing();

public:
    /**
     * print instructions
     * ask if they would like to create a new game or load game
     * load or create game
     * while there are enemies left and player is not dead
     *     give player game options
     *     if option = 1
     *      go shop
     *     if option = 2
     *      go fight
     *      if winner = player ==> congratz player
     *      if winner = null ==> player is wimpy and ran away
     *      else winner is enemy ==> game over :/
     *     if option = 3
     *      view character sheet
     *     if option = 4
     *      save game
     *     if option = 5
     *      exit game
     * print credits
     * end game
     */
    void gameBegin();

    /**
     * delete the referenced player
     */
    ~GladiatorGame();

};


#endif //LETSFIGHT_GLADIATORGAME_H
