//
// Created by Chandler on 11/1/2016.
//

#include "Store.h"
#include <fstream>
#include <sstream>
#include "Armor.h"
#include "Glove.h"
#include "Shield.h"
#include "Weapon.h"
#include "HealthPot.h"

using namespace std;

Store::Store()
{
    // default constructor
}

void Store::createItems()
{
    //  Open an input file in read mode
    ifstream infile;
    infile.open("./items.txt", ios::in);
    if (!infile)
    {
        cout << "Could not open input file!" << endl;
        return;
    }

// Parse tokens from lines seperated by | from input file
    string type, name, abilityPowStr, buyPriceStr;
    string line;
    cout << "Getting Items..." << endl;
    while (getline(infile,line))
    {
        // Create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(line);
        getline(lineStream, type, '|');
        getline(lineStream, name, '|');
        getline(lineStream, abilityPowStr, '|');
        getline(lineStream, buyPriceStr, '|');


        //  Change nessesary values to correct input
        int abilityPow = stoi(abilityPowStr);
        int buyPrice = stoi(buyPriceStr);
        //  Determine what type of item and create item
        if (type == "Armor")
        {
            shop.push_back(new Armor(name, abilityPow, buyPrice));
        }
        else if (type == "Shield")
        {
            shop.push_back(new Shield(name, abilityPow, buyPrice));
        }
        else if (type == "Glove")
        {
            shop.push_back(new Glove(name, abilityPow, buyPrice));
        }
        else if (type == "Weapon")
        {
            shop.push_back(new Weapon(name, abilityPow, buyPrice));
        }
        else if (type == "Health Pot")
        {
            shop.push_back(new HealthPot(name, abilityPow, buyPrice));
        }
        else
        {
            cout << "Item " << name << "could not be created." << endl;
        }
    }

    //  Clean Up
    infile.close();
    totalCoins = 0;
}

void Store::showStore()
{
    //cout << "Number Type   Name  Ability   Ability power  buy price  sell price"
    cout << "STORE: " << endl;
    cout << itemKey << endl;
    //for each item in store
    int numOfItems = shop.size();
    for (int index = 0; index < numOfItems; index++)
    {
        //print item number
        string number = to_string(index + 1);
        number.resize(2,' ');
        cout << number << "       ";
        //print each item
        Salable* item = shop[index];
        cout << *item << endl;
    }
    //  Spacing
    cout << "\n\n" << endl;
    return;
}


void Store::addToCart()
{
    //  Initialize variables
    Salable* item;
    bool gettingChoice = true;
    string choiceStr;
    int choice;
    int maxChoice = shop.size();
    while (gettingChoice)
    {
        //get item number form user
        cout << "Enter the number of the item you would like to add to the cart: ";
        cin >> choiceStr;
        //check if valid
        //check if int
        //check if in range
        try {
            choice = stoi(choiceStr);
        }
        catch(const invalid_argument)
        {
            cout << choiceStr << " is not a valid option.\n" << endl;
            continue;
        }
        if (choice > maxChoice or choice < 1)
        {
            cout << "There is no item number " << choice << " in the store." << endl;
            cout << "Please enter another number.\n" << endl;
        } else{
            gettingChoice = false;
        }
    }
    //  find item in store
    choice-=1;
    item = shop.at(choice);
    shop.erase(shop.begin()+choice);
    //  add item to cart
    storage.push_back(item);
    //  add item buy price to total coins
    totalCoins += item->getBuyPrice();
    //  Spacing
    cout << "\n" << endl;
    return;
}

void Store::removeFromCart()
{
    //initialize variables
    Salable* item;
    bool gettingChoice = true;
    string choiceStr;
    int choice;
    int maxChoice = storage.size();
    //check if items are in cart
    if (maxChoice == 0)
    {
        cout << "There are no items in the cart to remove.\n\n" << endl;
        gettingChoice = false;
        return;
    }
    //get item number to remove
    while (gettingChoice)
    {
        //get item number form user
        cout << "Enter the number of the item you would like to remove from the cart: ";
        cin >> choiceStr;
        //check if valid
        //check if int
        //check if in range
        choice  = stoi(choiceStr);
        if (choice > maxChoice)
        {
            cout << "There is no item number " << choice << " in the cart." << endl;
            cout << "Please enter another number.\n" << endl;
        } else{
            gettingChoice = false;
        }

    }
    //  find item in cart
    choice-=1;
    item = storage.at(choice);
    storage.erase(storage.begin()+choice);
    //  add item to cart
    shop.push_back(item);
    //  add item buy price to total coins
    totalCoins -= item->getBuyPrice();
    //  Spacing
    cout << "\n" << endl;
}

void Store::resetCoins()
{
    //set total coins = 0
    totalCoins = 0;
    return;
}

void Store::emptyCart()
{
    //clear cart storage.clear()
    storage.clear();
    return;
}

void Store::itemReturn(Salable* item)
{
    //add item to shop
    shop.push_back(item);
    return;
}

void Store::showCart(int playerCoins)
{
    //declare cart
    cout << "CART:                 Total Price: " << totalCoins << "        Player Coins: " << playerCoins << endl;
    //show cart
    showStorage();
}

vector<Salable *> Store::getCart() {
    return storage;
}

void Store::saveItems() {
    ofstream save_file;
    save_file.open("./save_file.txt", ios_base::app);
    if (!save_file)
    {
        cout << "Could not open save_file.tx file" << endl;
        cout << "Could not save game." << endl;
        return;
    }
    //iterate through store and save items
    //store(loaction)|type|name|AP|BP|
    for (int index = 0; index < shop.size(); index++)
    {
        save_file << "store|" << shop[index]->getType() << "|" << shop[index]->getName() << "|" << shop[index]->getAbilityPower() << "|" << shop[index]->getBuyPrice() << "|\r\n" << endl;
    }
    save_file.close();
}

Store::~Store()
{
    //clean up

    for (int index = 0; index < storage.size(); index++)
    {
        Salable *item = storage[index];
        delete(item);
    }

    for (int index = 0; index < shop.size(); index++)
    {
        Salable *item = shop[index];
        delete(item);
    }
}

