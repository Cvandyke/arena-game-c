//
// Created by Chandler on 11/1/2016.
//

#include "Glove.h"

Glove::Glove()
{
    //set coin bonus to 0
    coinBonus = 0;
}

Glove::Glove(string name, int coinBonus, int buyPrice): Salable(name, buyPrice)
{
    //set this coinbonus = coinBonus
    this->coinBonus = coinBonus;
    //set type =  Glove
    type = "Glove";
    //set ability = Coin Bonus
    ability = "Coin Bonus";
}


int Glove::getAbilityPower()
{
    //return coinbonus
    return coinBonus;
}

Glove::~Glove()
{
    coinBonus = 0;
    name = "";
    buyPrice = 0;
    sellPrice = 0;
}
