//
// Created by Chandler on 11/1/2016.
//

#ifndef LETSFIGHT_ARMOR_H
#define LETSFIGHT_ARMOR_H

#include "Salable.h"

class Armor: public Salable
{
private:
    int armor;
public:

    /**
     * defualt constructor
     * @return
     */
    Armor();

    /**
     * set type, name, ability, armor, buyprice
     * @param name
     * @param armor
     * @param buyPrice
     * @return
     */
    Armor(string name, int armor, int buyPrice);

    /**
     * get damage
     * @return damage
     */
    int getAbilityPower();

    /**
     * name to null
     * armor to 0
     * buyprice to 0
     * sell price to 0
     */
    ~Armor();
};


#endif //LETSFIGHT_ARMOR_H
