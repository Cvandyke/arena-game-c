//
// Created by Chandler on 11/27/2016.
//

#ifndef LETSFIGHT_INVENTORY_H
#define LETSFIGHT_INVENTORY_H

#include "BaseStorage.h"
#include "Salable.h"
#include "HealthPot.h"
#include "Weapon.h"
#include "Shield.h"
#include "Glove.h"
#include "Armor.h"
#include <fstream>

class Inventory : public BaseStorage
{
public:

    /**
     * default constructor
     * set total coins to 200
     * @return
     */
    Inventory();
    /**
     * add item to storage
     * @param items
     */
    void addItem(Salable* item);

    /**
     * print inventory
     * ask what item to sell
     * get item
     * add sell price to total coins
     * remove item ability from player abilities
     * remove item form inventory
     * @return item
     */
    Salable* sellItem();

    /**
     * iterate through storage
     * show all items that are type HealthPot
     * ask what health pot to use
     * remove health pot from storage
     * return health pot
     */
    Salable* getPot ();

    /**
     * add coinstoadd to coins total
     * @param coinsToAdd
     */
    void addCoins(int coinsToAdd);

    /**
     * remove coinstoremove from total coins
     * @param coinsToRemove
     */
    void removeCoins(int coinsToRemove);

    /**
     * call size on storage
     * @return size
     */
    int numberOfItems();

    /**
     * iterate through to save items
     */
    void saveItems();

    /**
     * iterate through to delete all items
     */
    ~Inventory();

};


#endif //LETSFIGHT_INVENTORY_H
