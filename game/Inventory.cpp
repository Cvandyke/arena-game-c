//
// Created by Chandler on 11/27/2016.
//

#include "Inventory.h"

Inventory::Inventory ()
{
    //set total coins to 200
    totalCoins = 200;
}

void Inventory::addItem(Salable* item) {
    //add item to storage
    storage.push_back(item);
}

Salable* Inventory::sellItem() {
    //print storage
    showStorage();
    //get item shoice
    Salable* item;
    bool gettingChoice = true;
    string choiceStr;
    int choice;
    int maxChoice = storage.size();
    while (gettingChoice)
    {
        //get item number form user
        cout << "Enter the number of the item you would like to sell: ";
        cin >> choiceStr;
        //check if valid
        //check if int
        //check if in range
        try {
            choice = stoi(choiceStr);
        }
        catch(const invalid_argument)
        {
            cout << choiceStr << " is not a valid option.\n" << endl;
            continue;
        }
        if (choice > maxChoice or choice <= 0)
        {
            cout << "There is no item number " << choice << " in your inventory." << endl;
            cout << "Please enter another number.\n" << endl;
        } else{
            gettingChoice = false;
        }
    }
    //  find item in store
    choice-=1;
    item = storage.at(choice);
    storage.erase(storage.begin()+choice);
    //  add item buy price to total coins
    totalCoins += item->getSellPrice();
    //  Spacing
    cout << "\n" << endl;

    return item;
}

Salable *Inventory::getPot() {
    //initiate integers to show how many pots of each type there are
    int basic = 0;
    int lipo = 0;
    int trump = 0;

    //itereate through storage to find pots and increase them according to availabilty
    for (int index = 0; index < storage.size(); index++)
    {
        if (storage[index]->getType() == "Health Pot" )
        {
            if (storage[index]->getAbilityPower() == 40)
                basic+= 1;
            else if (storage[index]->getAbilityPower() == 85)
                lipo+= 1;
            else if (storage[index]->getAbilityPower() == 150)
                trump+= 1;
        }
    }
    int total = basic + lipo + trump;
    if (total == 0)
    {
        cout << "You do not have any health pots. \nGo to the store to buy some" << endl;
        return nullptr;
    }
    //print each usable type
    cout << "\nUsable Pots:" << endl;
    cout << "1. Basic Pot: " << basic << endl;
    cout << "2. Lipo Pot: " << lipo << endl;
    cout << "3. Trump Pot: " << trump << endl;

    //ask user which to use
    string choiceStr;
    int choice;
    bool gettingChoice = true;
    while (gettingChoice) {
        //get item number form user
        cout << "Enter the number of the pot you would like to use: ";
        cin >> choiceStr;
        //check if valid
        //check if int
        //check if in range
        try {
            choice = stoi(choiceStr);
        }
        catch (const invalid_argument) {
            cout << choiceStr << " is not a valid option.\n" << endl;
            continue;
        }
        if (1 > choice > 3) {
            cout << choice << " is not a valid pot number." << endl;
            cout << "Please enter another number.\n" << endl;
        } else {
            gettingChoice = false;
        }
    }

    //find item in storage, remove pot
    int itemPower;
    if (choice == 1)
        itemPower = 40;
    else if (choice == 2)
        itemPower = 85;
    else if (choice == 3)
        itemPower = 150;
    Salable* item = nullptr;
    for (int index = 0; index < storage.size(); index++)
    {
        if (storage[index]->getType() == "Health Pot" and storage[index]->getAbilityPower() == itemPower)
        {
            item = storage[index];
            storage.erase(storage.begin()+index);
            break;
        }
    }
    if (item == nullptr)
    {
        cout << "You chose to use an empty pot." << endl;
        cout << "You look like a fool drinking from an empty flask." << endl;
        cout << "You lose your turn!" << endl;
    }

    //return pot
    return item;
}

void Inventory::addCoins(int coinsToAdd) {
    totalCoins+=coinsToAdd;
    return;
}

void Inventory::removeCoins(int coinsToRemove) {
    totalCoins-=coinsToRemove;
    return;
}

int Inventory::numberOfItems() {
    int number = storage.size();
    return number;
}

void Inventory::saveItems() {
    ofstream save_file;
    save_file.open("./save_file.txt", ios_base::app);
    if (!save_file)
    {
        cout << "Could not open save_file.tx file" << endl;
        cout << "Could not save game." << endl;
        return;
    }
    //iterate through inventory and save items
    //player(loaction)|type|name|AP|BP|
    for (int index = 0; index < storage.size(); index++)
    {
        save_file << "player|" << storage[index]->getType() << "|" << storage[index]->getName() << "|" << storage[index]->getAbilityPower() << "|" << storage[index]->getBuyPrice() << "|\r\n" << endl;
    }
    save_file.close();
}


Inventory::~Inventory() {
    //clean up
    //itereate through to delete items in storage
    for (int index = 0; index < storage.size(); index++)
    {
        Salable *item = storage[index];
        delete(item);
    }

}


