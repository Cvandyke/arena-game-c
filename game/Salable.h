//
// Created by Chandler on 10/31/2016.
//

#ifndef LETSFIGHT_Salable_H
#define LETSFIGHT_Salable_H

#include <iostream>
#include <ostream>

using namespace std;

class Salable {
protected:
    string type = "Base";
    string name;
    //for printing purposes
    string ability = "N/A";
    int buyPrice;
    int sellPrice;

private:
    void setSellPrice(int sellPrice);

public:
    /**
     * create base item
     * @return
     */
    Salable ();

    /**
     * assign type, name, buyPrice, ability, and sellPrice
     * @param name
     * @param buyPrice
     * @param healthAspect
     * @return
     */
    Salable(string name, int buyPrice);


    /**
     * get buy price
     * @return buy price
     */
    int getBuyPrice() const;

    /**
     * get sell price
     * @return sell price
     */
    int getSellPrice() const;


    /**
     * get ability power
     * @return
     */
    virtual int getAbilityPower() = 0;


    /**
     * add to string in format:
     * "Type  Name  Special Ability   Ability Power   Buy price   Sell Price"
     * add string to ouput
     * @param output
     * @param BI
     * @return output
     */
    friend ostream&operator<<(ostream& output, Salable& SI);

    /**
     * get type
     * @return type
     */
    const string &getType() const;

    /**
     * return name
     * @return
     */
    string getName();


};


#endif //LETSFIGHT_Salable_H
