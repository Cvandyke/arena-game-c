//
// Created by Chandler on 10/31/2016.
//

#include "Salable.h"

using namespace std;

Salable::Salable ()
{
    return;
}

Salable::Salable(string name, int buyPrice)
{
    //set this name = name
    this-> name = name;
    //set this buy price = buyprice
    this-> buyPrice = buyPrice;
    //set sell price
    setSellPrice(buyPrice);
    return;
}


int Salable::getBuyPrice() const {
    //return buyprice
    return buyPrice;
}

int Salable::getSellPrice() const {
    //return sellPrice
    return sellPrice;
}


ostream&operator<<(ostream& output, Salable& SI)
{
    //format values of item to fit a format for printing
    //set variable to resize
    //resize to a certian length filling extra in with " "
    //type
    string strType = SI.type;
    strType.resize(12, ' ');
    //name
    string strName = SI.name;
    strName.resize(12, ' ');
    //ability
    string strAbility = SI.ability;
    strAbility.resize(14, ' ');
    //ability power
    string ap = to_string(SI.getAbilityPower());
    ap.resize(10, ' ');
    //buy price
    string bp = to_string(SI.buyPrice);
    bp.resize(11, ' ');
    //sell price
    string sp = to_string(SI.sellPrice);
    sp.resize(12, ' ');


    //put everything together
    output << strType << strName << strAbility  << ap << bp << sp;
    return output;

}

void Salable::setSellPrice(int buyPrice)
{
    //set sell price to 65% of original price
    this->sellPrice = buyPrice * .65;
    return;
}


int Salable::getAbilityPower()
{
    //just for space saving
    return 0;
}

const string &Salable::getType() const {
    return type;
}

string Salable::getName() {
    return name;
}
