//
// Created by Chandler on 11/27/2016.
//

#include "BaseHero.h"

BaseHero::BaseHero()
{
    //null coins and coin bonus
}

BaseHero::BaseHero(string name, int coins, int health, int armor, int damage, int coinBonus): BasePerson(name, health, armor, damage)
{
    //add coins to purse
    purse.addCoins(coins);
    //set coin bonus
    this-> coinBonus = coinBonus;
}

int BaseHero::getCoinBonus() {
    //return coinBonus
    return coinBonus;
}

Salable *BaseHero::sellItemFromPurse() {
    //call sell item on purse
    if (purse.numberOfItems() == 0)
    {
        cout << "There are no items in your inventory to sell." << endl;
        return nullptr;
    }
    Salable* selling = purse.sellItem();
    if (selling->getType() == "Armor")
        armor-=selling->getAbilityPower();
    else if (selling->getType() == "Glove")
        coinBonus-=selling->getAbilityPower();
    else if (selling->getType() == "Shield")
        health-=selling->getAbilityPower();
    else if (selling->getType() == "Weapon")
        damage-=selling->getAbilityPower();
    //return item retrieved from purse
    return selling;
}

void BaseHero::addItemsToPurse(vector<Salable *> items) {
    //iterate through items
    Salable* item = nullptr;
    int cost = 0;
    int numOfItems = items.size();
    for (int index = 0; index < numOfItems; index++){
        item = items.at(0);
        items.erase(items.begin());
        if (item->getType() == "Armor")
            armor+=item->getAbilityPower();
        else if (item->getType() == "Glove")
            coinBonus+=item->getAbilityPower();
        else if (item->getType() == "Shield")
            health+=item->getAbilityPower();
        else if (item->getType() == "Weapon")
            damage+=item->getAbilityPower();
        cost += item->getBuyPrice();
        //add item to purse
        purse.addItem(item);
    }
    purse.removeCoins(cost);
}

void BaseHero::addCoinsToPurse(int coinsToAdd) {
    //call add coins to purse
    purse.addCoins(coinsToAdd);
}


void BaseHero::addCoinBonus(int coinBonus) {
    this->coinBonus += coinBonus;
}

void BaseHero::removeCoinBonus(int coinBonus) {
    this->coinBonus-=coinBonus;
    return;
}

void BaseHero::removeCoinsFromPurse(int coinsToRemove) {
    //call remove coins from purse
    purse.removeCoins(coinsToRemove);
}

void BaseHero::useHealthPot() {
    //call getpot on purse
    Salable* pot = purse.getPot();
    //player health += healthPot.getability power
    if (pot != nullptr) {
        health += pot->getAbilityPower();
    }
    //delete health pot
    delete(pot);

}

void BaseHero::showInventory() {
    //call showstorage on purse
    purse.showStorage();
}

void BaseHero::showPlayerStats() {
    //print stats in following format"
    //Name:  ....
    cout << "\n\nName: " << name << endl;
    //Coins: ....
    cout << "Coins: " << getCoins() << endl;
    //Number of Items: ...
    cout << "Items: " << purse.numberOfItems()<< endl;
    //Health: ....
    cout << "Health: " << health << endl;
    //Damage: ....
    cout << "Damage: " << damage << endl;
    //Armor: ....
    cout << "Armor: " << armor << endl;
    //Coin Bonus: ..."
    cout << "Coin Bonus: " << coinBonus << "\n" <<endl;
}

int BaseHero::getCoins() {
    int coins = purse.getTotalCoins();
    return coins;
}




BaseHero::~BaseHero() {
    //set all values to null
}

void BaseHero::savePurse() {
    purse.saveItems();
}

void BaseHero::addItem(Salable *item) {
    //adjust stats
    if (item->getType() == "Armor")
        armor+=item->getAbilityPower();
    else if (item->getType() == "Glove")
        coinBonus+=item->getAbilityPower();
    else if (item->getType() == "Shield")
        health+=item->getAbilityPower();
    else if (item->getType() == "Weapon")
        damage+=item->getAbilityPower();
    //add item to purse
    purse.addItem(item);
}


