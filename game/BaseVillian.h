//
// Created by Chandler on 11/30/2016.
//

#ifndef LETSFIGHT_BASEVILLIAN_H
#define LETSFIGHT_BASEVILLIAN_H

#include "BasePerson.h"


class BaseVillian: public BasePerson {
private:
    int coins;
    int fullHealth;
public:
    /**
     * itiate state
     * @return
     */
    BaseVillian();

    /**
     * initiate all vaieables
     * @param name
     * @param health
     * @param armor
     * @param damage
     * @return
     */
    BaseVillian(string name, int health, int armor, int damage, int coins);

    /**
     * return health = full health
     */
    void heal();

    /**
     * return coins
     * @return
     */
    int getCoins();


};


#endif //LETSFIGHT_BASEVILLIAN_H
