//
// Created by Chandler on 10/31/2016.
//

#ifndef LETSFIGHT_BASESTORAGE_H
#define LETSFIGHT_BASESTORAGE_H

#include <vector>
#include <iostream>
#include <ostream>
#include "Salable.h"

using namespace std;

class BaseStorage {

protected:
    string itemKey = "Number   Type        Name        Ability       Power     Buy Price  Sell Price";
    vector<Salable*> storage;
    int totalCoins;

public:
    /**
     * default constructor
     *
     * @return
     */
    BaseStorage();

    /**
    * print out a list of the storages items
    * format: "Number Type   Name  Ability   Ability power  buy price  sell price"
    * iterate through items in storage and print
    */
    void showStorage ();


    /**
     * get total coins
     * @return total coins
     */
    int getTotalCoins();

};


#endif //LETSFIGHT_BASESTORAGE_H
