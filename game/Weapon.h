//
// Created by Chandler on 11/1/2016.
//

#ifndef LETSFIGHT_WEAPON_H
#define LETSFIGHT_WEAPON_H

#include "Salable.h"

class Weapon: public Salable
{
protected:
    int damage;

public:
    /**
     * defualt constructor
     * @return
     */
    Weapon();

    /**
     * special constructor
     * set type, name, ability, damage, buyprice
     * @param name
     * @param damage
     * @param buyPrice
     * @return
     */
    Weapon(string name, int damage, int buyPrice);

    /**
     * get damage
     * @return damage
     */
    int getAbilityPower();

    /**
     * name to null
     * damage to 0
     * buyprice to 0
     * sell price to 0
     */
    ~Weapon();
};


#endif //LETSFIGHT_WEAPON_H
