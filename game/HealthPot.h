//
// Created by Chandler on 11/24/2016.
//

#ifndef LETSFIGHT_HEALTHPOT_H
#define LETSFIGHT_HEALTHPOT_H

#include "Salable.h"

class HealthPot: public Salable
{
private:
    int healthRegen;
public:

    /**
     * defualt constructor
     * @return
     */
    HealthPot();

    /**
     * set type, name, ability, armor, buyprice
     * @param name
     * @param armor
     * @param buyPrice
     * @return
     */
    HealthPot(string name, int healthRegen, int buyPrice);

    /**
     * get HealthRegen
     * @return HealthRegen
     */
    int getAbilityPower();

    /**
     * name to null
     * health regen to 0
     * buyprice to 0
     * sell price to 0
     */
    ~HealthPot();
};


#endif //LETSFIGHT_HEALTHPOT_H
