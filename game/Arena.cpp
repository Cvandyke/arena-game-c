//
// Created by Chandler on 12/4/2016.
//

#include "Arena.h"
#include <fstream>
#include <iostream>
#include <sstream>

int Arena::getRandomNumber() {
    //get random number
    srand(time(NULL));
    int number;
    number = rand();

    //return 0 if even or 1 if odd
    number = number % 2;
    return number;
}

int Arena::playerOptions() {

    //cout player options and get user input
    string choiceStr;
    int choice;
    bool gettingChoice = true;
    while (gettingChoice)
    {
        //get item number form user
        cout << "What would you like to do?"<<endl;
        cout << "1) attack\n2) heal\n3) run\nChoice: ";
        cin >> choiceStr;
        //check if valid
        //check if int
        //check if in range
        try {
            choice = stoi(choiceStr);
        }
        catch(const invalid_argument)
        {
            cout << choiceStr << " is not a valid option.\n" << endl;
            continue;
        }
        if (choice <= 0 or choice >= 4)
        {
            cout << choice << " is not a vlid option." << endl;
            cout << "Please enter 1, 2, or 3.\n" << endl;
        } else{
            gettingChoice = false;
        }
    }
    return choice;
}

void Arena::run(BaseVillian* villian) {
    //heal current monster
    villian->heal();
    //exit arena
}

bool Arena::playerAttacking(BasePerson* attPlayer, BasePerson* deffPlayer) {
    //get attPlayer damage
    int damage = attPlayer->getDamage();
    //get deffPlayer armor
    int armor = deffPlayer->getArmor();
    //figure out final attack damage
    damage = damage - (armor*.35);
    bool dead = false;
    cout << attPlayer->getName() << " attacks " << deffPlayer->getName() << endl;
    if (damage <= 0)
    {
        cout << deffPlayer->getName() << " takes no damage!" << endl;
        cout << deffPlayer->getName() << "'s armor is too strong!" << endl;
    }else {
        cout << deffPlayer->getName() << " takes " << damage << " damage." << endl;
        //defPlayr takes final attack damage
        dead = deffPlayer->getAttacked(damage);
    }
    //return true if dead, else false
    return dead;
}

string Arena::fight(BaseHero* player) {
    //get starting turn number
    int start = getRandomNumber();
    BaseVillian* monster = enemies[level];
    //winner  = null
    string winner = "null";
    //while winner = null
    while (winner == "null") {
        //  if turn number % 2  = 0: ==> players turn
        start += 1;
        cout << "\n\n" << endl;
        cout << player->getName() << " Health: " << to_string(player->getHealth()) << endl;
        cout << monster->getName() << " Health: " << to_string(monster->getHealth()) << endl;
        if (start % 2 == 0) {
            // call player options
            int option = playerOptions();
            // if option 1 ==> player attacks enemy
            if (option == 1) {
                if (playerAttacking(player, monster)) {
                    //if enemy dead ==> winner = "Player"
                    winner = player->getName();
                    player->addCoinsToPurse(monster->getCoins());
                    player->addCoinsToPurse(player->getCoinBonus());
                    level += 1;
                }

            }
                // if option 2 ==> player calls use health pot
            else if (option == 2) {
                player->useHealthPot();
            }
                // if option 3 ==> call run and winner = none
            else if (option == 3) {
                run(monster);
                winner = "none";
            }
        } else{
            //  if turn number % 2 = 1: ==> enemy turn
            //  enemy attacks player
            if (playerAttacking(monster, player))
                //if player dead ==> winner = "Enemy's name"
                winner = monster->getName();
        }

    }
    return winner;
}

void Arena::setLevel(int levelSet) {
    //level = levelset
    this->level = levelSet;
    return;
}

int Arena::enemiesLeft() {
    //size of enemies - level + 1 = enemies left
    int left;
    left = enemies.size() - level;
    return left;
}

Arena::~Arena() {
    for (int index = 0; index < enemies.size(); index++)
    {
        BaseVillian* villian = enemies[index];
        delete(villian);
    }
}


void Arena::createEnemies() {
    // Open an input file in read mode
    ifstream infile;
    infile.open("./characters.txt", ios::in);
    if (!infile) {
        cout << "Could not open input file" << endl;
        return;
    }
    string name, healthStr, armorStr, damageStr, coinsStr;
    string line;
    while (getline(infile, line)) {
        // Create a string stream from the line and parse its tokens usiong a | delimeter
        istringstream lineStream(line);
        getline(lineStream, name, '|');
        getline(lineStream, healthStr, '|');
        getline(lineStream, armorStr, '|');
        getline(lineStream, damageStr, '|');
        getline(lineStream, coinsStr, '|');
        int health = stoi(healthStr);
        int armor = stoi(armorStr);
        int damage = stoi(damageStr);
        int coins = stoi(coinsStr);
        enemies.push_back(new BaseVillian(name, health, armor, damage, coins));
    }
}

int Arena::getLevel() {
    return level;
}
