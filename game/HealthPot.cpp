//
// Created by Chandler on 11/24/2016.
//

#include "HealthPot.h"

HealthPot::HealthPot()
{
    //HealthPot = 0;
}

HealthPot::HealthPot(string name, int healthRegen, int buyPrice): Salable(name, buyPrice)
{
    //set this HealthPot = int HealthPot
    this->healthRegen = healthRegen;
    //set type = HealthPot
    type = "Health Pot";
    //set ability = "HealthPot
    ability = "Health Regen";
}

int HealthPot::getAbilityPower()
{
    //return HealthPot
    return healthRegen;
}


HealthPot::~HealthPot()
{
    healthRegen = 0;
    name = "";
    buyPrice = 0;
    sellPrice = 0;
}