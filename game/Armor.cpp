//
// Created by Chandler on 11/1/2016.
//

#include "Armor.h"

Armor::Armor()
{
    //armor = 0;
}

Armor::Armor(string name, int armor, int buyPrice): Salable(name, buyPrice)
{
    //set this armor = int armor
    this->armor = armor;
    //set type = armor
    type = "Armor";
    //set ability = "Armor
    ability = "Armor";
}

int Armor::getAbilityPower()
{
    //return armor
    return armor;
}


Armor::~Armor()
{
    armor = 0;
    name = "";
    buyPrice = 0;
    sellPrice = 0;
}