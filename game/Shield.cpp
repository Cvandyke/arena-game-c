//
// Created by Chandler on 11/1/2016.
//

#include "Shield.h"

Shield::Shield()
{
    //set health = 0
    health = 0;
}

Shield::Shield(string name, int health, int buyPrice): Salable(name, buyPrice)
{
    //set this health = health
    this->health = health;
    //set type = Shield
    type = "Shield";
    //set ability = Health
    ability = "Health";
}

int Shield::getAbilityPower()
{
    //return health
    return health;
}

Shield::~Shield()
{
    health = 0;
    name = "";
    buyPrice = 0;
    sellPrice = 0;
}