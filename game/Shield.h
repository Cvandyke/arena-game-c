//
// Created by Chandler on 11/1/2016.
//

#ifndef LETSFIGHT_SHIELD_H
#define LETSFIGHT_SHIELD_H

#include "Salable.h"

class Shield : public Salable
{
protected:
    int health;

public:
    /**
     * defualt constructor
     * @return
     */
    Shield();

    /**
     * set type, name, ability, damage, buyprice
     * @param name
     * @param health
     * @param buy Price
     * @return
     */
    Shield(string name, int health, int buyPrice);

    /**
     * get health
     * @return health
     */
    int getAbilityPower();

    /**
     * name to null
     * health to 0
     * buyprice to 0
     * sell price to 0
     */
    ~Shield();
};


#endif //LETSFIGHT_SHIELD_H
