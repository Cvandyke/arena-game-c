//
// Created by Chandler on 11/1/2016.
//

#include "Weapon.h"

Weapon::Weapon()
{
    //set damage = 0
}

Weapon::Weapon(string name, int damage, int buyPrice): Salable(name, buyPrice)
{
    //set this damage = 0
    this->damage = damage;
    //set type = weapon
    type = "Weapon";
    //set ability = Damage
    ability = "Damage";
}

int Weapon::getAbilityPower()
{
    //return damage
    return damage;
}

Weapon::~Weapon()
{
    damage = 0;
    name = "";
    buyPrice = 0;
    sellPrice = 0;
}