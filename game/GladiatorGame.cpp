//
// Created by Chandler on 12/4/2016.
//

#include "GladiatorGame.h"
#include <fstream>
#include <iostream>
#include <sstream>

void GladiatorGame::printInstructions() {
    //cout game instructions as defined in readME
    cout << "INSTRUCTIONS:" << endl;
    cout << "This game will consist of a you being able to fight in an arena type game, \n"
            "moving from opponent to opponent to a boss. You can also shop in the store. \n"
            "The store is where  can buy better items with the coins you start with aswell\n"
            "as earn in the arena.  After defeating an enemy, you can go buy items to improve\n"
            "your stats or you  can buy health potions(healht pots) to get health \n"
            "back in the game. Buying items will be required in order for you to make it to\n"
            "later rounds. You will gain coins after killing an oppenent.  Coins can be spent at \n"
            "the store.  After battling an enemy, you will have an option to continue \n"
            "fighting, go to the store, save the game, or exit.  When you are fighting an \n"
            "enemy, you will take turns with the enemy.  During each turn you can \n"
            "decide to attack, heal, or run away.  If you choose to run, the enemy's \n"
            "health will be restored. In order to heal, you must first of had purchased \n"
            "health pots at the store.  Attacking will damage the enemy.  You are able to\n"
            "continue as long as you still have health.\n\n"
            "Good Luck!!!\n\n  " << endl;

}

bool GladiatorGame::loadGame() {
    //get lines in save_game.txt
    bool saved = false;
    ifstream save_file;
    save_file.open("./save_file.txt", ios::in);
    if (!save_file)
    {
        cout << "Could not open save_file" << endl;
        return saved;
    }
    string line;
    int lines = 0;
    //Check to see if something is saved
    // Parse tokens from lines separated by | from input file
    string charName, levelStr, coinsStr, location, type, itemName, apStr, bpStr;
    while (getline(save_file, line))
    {
        // Create a string stream from the line and parse its tokens usiong a | delimeter
        //first line is character information
        //  name, level, and coins ==> rest of parameters are default to new characters
        istringstream lineStream(line);
        if (lines == 0) {
            if (line.size() < 3)
            {
                return saved;
            }
            getline(lineStream, charName, '|');
            getline(lineStream, levelStr, '|');
            getline(lineStream, coinsStr, '|');
            player = new BaseHero(charName, stoi(coinsStr), 200, 10, 20, 10);
            //  set arena level = level in given line above
            coliseum.setLevel(stoi(levelStr));
        }
            //all the rest of the lines are items

        else if (line.size() > 3){
            getline(lineStream, location, '|');
            getline(lineStream, type, '|');
            getline(lineStream, itemName, '|');
            getline(lineStream, apStr, '|');
            getline(lineStream, bpStr, '|');

            //first token is location ==> player or store
            //createitem with infromation given (tokens 2-4)
            //add item to location (store or player)
            if (location == "store")
            {
                int abilityPow = stoi(apStr);
                int buyPrice = stoi(bpStr);
                //  Determine what type of item and create item
                if (type == "Armor")
                {
                    store.itemReturn(new Armor(itemName, abilityPow, buyPrice));
                }
                else if (type == "Shield")
                {
                    store.itemReturn(new Shield(itemName, abilityPow, buyPrice));
                }
                else if (type == "Glove")
                {
                    store.itemReturn(new Glove(itemName, abilityPow, buyPrice));
                }
                else if (type == "Weapon")
                {
                    store.itemReturn(new Weapon(itemName, abilityPow, buyPrice));
                }
                else if (type == "Health Pot")
                {
                    store.itemReturn(new HealthPot(itemName, abilityPow, buyPrice));
                }
                else
                {
                    cout << "Item " << itemName << "could not be created." << endl;
                }
            } else{
                //as items are added to player, player's stats will be updated with the player.additem function
                int abilityPow = stoi(apStr);
                int buyPrice = stoi(bpStr);
                //  Determine what type of item and create item
                if (type == "Armor")
                {
                    player->addItem(new Armor(itemName, abilityPow, buyPrice));
                }
                else if (type == "Shield")
                {
                    player->addItem(new Shield(itemName, abilityPow, buyPrice));
                }
                else if (type == "Glove")
                {
                    player->addItem(new Glove(itemName, abilityPow, buyPrice));
                }
                else if (type == "Weapon")
                {
                    player->addItem(new Weapon(itemName, abilityPow, buyPrice));
                }
                else if (type == "Health Pot")
                {
                    player->addItem(new HealthPot(itemName, abilityPow, buyPrice));
                }
                else
                {
                    cout << "Item " << itemName << "could not be created." << endl;
                }
            }
        }
        lines++;
    }
    saved = true;
    return saved;

}

void GladiatorGame::createGame() {
    //create new shop
    store.createItems();
    //create new player
    string name;
    cout << "Please enter your name: ";
    cin >> name;
    player = new BaseHero(name, 100, 200, 10, 20, 10);
}

int GladiatorGame::gameOptions() {
    bool validOption = true;
    string choiceStr;
    int choice;
    //while option is not valid
    while (validOption) {
        //  ask for number of option that they would like to execute
        //  1: fight
        //  2: go shopping
        //  3: view charcter stats
        //  4: save game
        //  5: exit game
        cout << "Main Menu:" << endl;
        cout << "1) Go Shopping\n2) Fight\n3) View Character Sheet\n4) Save Game\n5) Exit Game" << endl;
        cout << "Enter number of what you would like to do:";
        cin >> choiceStr;
        //  stoi input
        //  check if is number
        try {
            choice = stoi(choiceStr);
        }
        catch(const invalid_argument)
        {
            cout << choiceStr << " is not a valid integer.\n" << endl;
            continue;
        }
        //  if input not 0 < input < 6
        if (choice <= 0 or choice >= 6) {
            //     input not in range
            cout << choice << " is not an option, please enter a number 1-5.\n" << endl;
            continue;
            //     continue
        }
        //  option is valid
        validOption = false;
    }
    //  return input
    return choice;
}

void GladiatorGame::shop() {
    bool shopping = true;
    int playerCOins = player->getCoins();

    //start shopping
    while (shopping)
    {
        //update coins in case of sell
        playerCOins = player->getCoins();
        //show store and cart
        store.showStore();
        store.showCart(playerCOins);
        bool gettingInstructions = true;
        int choice = 0;
        string choiceStr;
        //what does the user want to do
        while (gettingInstructions)
        {
            //explain options
            cout << "Enter the number you would like to do." << endl;
            cout << "1) add item to cart\n2) remove item from cart\n3) sell item\n4) check out" << endl;
            //get choice
            cin >> choiceStr;
            //check choice
            try {
                choice = stoi(choiceStr);
            }
            catch(const invalid_argument)
            {
                cout << choiceStr << " is not a valid option.\n" << endl;
                continue;
            }
            if (choice == 1 or choice == 2 or choice == 3 or choice == 4)
            {
                gettingInstructions = false;
            }
            else{
                cout << choice << " is not a valid option.\n" << endl;
            }
        }
        //add item to cart
        if (choice == 1)
        {
            store.addToCart();
        }
            //remove item from cart
        else if (choice == 2)
        {
            store.removeFromCart();
        }
        //sell item
        else if(choice == 3)
        {
            Salable* item = player->sellItemFromPurse();
            if (item != nullptr){
                store.itemReturn(item);
            }
        }
        //check out
        else if (choice == 4)
        {
            if (playerCOins >= store.getTotalCoins())
            {
                //pay for stuff
                playerCOins -= store.getTotalCoins();
                //reset store tab
                store.resetCoins();
                //exit shop
                cout << "Updating Inventory..." << endl;
                player->addItemsToPurse(store.getCart());
                store.emptyCart();
                shopping = false;
            }
            // not enough coins
            else
            {
                cout << "Sorry. You dont have enough coins to make this purchase." << endl;
                cout << "Please remove items until you have enough to cover the cost.\n\n" << endl;
            }
        }
        // in game, this function will add all items in the cart to the player inventory
        // before exiting.
    }

}

string GladiatorGame::fight(BaseHero *player) {
    //call comliseum.fight
    winner = coliseum.fight(player);
    return winner;
}

void GladiatorGame::characterStats(BaseHero *player) {
    //call on player to show stats
    player->showPlayerStats();
    return;
}

void GladiatorGame::save() {
    cout << "Attempting to save..." << endl;
    // Open an output file in write mode
    ofstream save_file;
    save_file.open("./save_file.txt");
    if (!save_file)
    {
        cout << "Could not open save_file.tx file" << endl;
        cout << "Could not save game." << endl;
        return;
    }
    //write to file line one ==> player name|Arena level|Player Coins|
    save_file << player->getName() << "|" << coliseum.getLevel() << "|" << player->getCoins() << "|\r\n" << endl;
    //close file
    save_file.close();
    //write to file all items in store ==> store(loaction)|type|name|AP|BP|
    store.saveItems();
    //write to file all items in inventory ==> inventory(location)|type|name|AP|BP|
    player->savePurse();
    cout << "Saved game!\n\n" << endl;


}

void GladiatorGame::printCredits() {
    //print closing information
    if (winner != "") {
        cout << "The winner is: " << winner << endl;
    }
    cout << "Thanks for playing!!!\nGame By: Chandler Van Dyke" << endl;
    //ie: winner and my name or something
}


void GladiatorGame::getGame() {
    //call printInstructions
    printInstructions();
    //while loading\creating
    bool gettingGame = true;
    string choiceStr;
    int choice;
    while (gettingGame) {
        //  ask if they would like to 1:load or 2:create
        cout << "What would you like to do?\n"
                "1) Load Game\n2) Create Game";
        cin >> choiceStr;
        //  check if input is int
        //  check if input is in range 1 or 2
        try {
            choice = stoi(choiceStr);
        }
        catch(const invalid_argument)
        {
            cout << choiceStr << " is not a valid integer.\n" << endl;
            continue;
        }
        //  if input not 0 < input < 3
        if (choice >= 3 or choice <= 0) {
            //     input not in range
            cout << choice << " is not an option, please enter a number 1 or 2.\n" << endl;
            continue;
            //     continue
        }
        //  if 1, check if there is data to load
        if (choice == 1) {
            //    if yes then load data
            bool data = loadGame();
            if (not data) {
                cout << "There was no saved data.\nCreating new game..." << endl;
                createGame();
            }
            gettingGame = false;
        }
            //  if 2, create new game
        else if (choice == 2)
        {
            createGame();
            gettingGame = false;
        }
        //  loading\creating = false
    }
}

void GladiatorGame::gameBegin() {
    getGame();
    coliseum.createEnemies();
    //gameover = false
    winner = "null";
    bool gameOver = false;
    int enemiesLeft;
    //while not gameover
    spacing();
    cout << "Welcome to the arena!" << endl;
    while(not gameOver) {
        //  option = gameoptions()
        int option = gameOptions();
        spacing();
        //  if option = 1
        if (option == 1)
        {
            //    shop()
            shop();
            spacing();
        }
        //  if option = 2
        else if (option == 2) {
            //    winner  = fight(player)
            winner = fight(player);
            //    if winner = player ==> congratz player
            if (winner == player->getName())
            {
                cout << "Congratulations! you made it through this round!" << endl;
            }
            //    if winner = null ==> player is wimpy and ran away
            else if (winner == "none")
            {
                cout << "Your wimpy kid... So disapointing..." << endl;
            }
            //    if winner = something else ==> gameover = False ==> player lost
            else if (winner != "null")
            {
                cout << "Your dead. X_X" << endl;
                gameOver = true;
                continue;
            }
            spacing();
        }
        //  if option = 3
        else if (option == 3){
        //    call characterStats
            player->showPlayerStats();
        }
        //  if option = 4
        else if (option == 4) {
            //    call save
            save();
        }
        //  if option = 5
        else if (option == 5) {
            //save and exit
            save();
            winner = "";
            gameOver = true;
            continue;
        }

        //  check if enemies are left
        enemiesLeft = coliseum.enemiesLeft();
        //  if yes ==> continue
        //  if no ==> gameover = true
        if (enemiesLeft == 0){
            gameOver = true;
            save();
            //  player won!!!
        }

    }
    //call credits
    printCredits();
    //end game
    return;
}

GladiatorGame::~GladiatorGame() {
    delete(player);
}

void GladiatorGame::spacing() {
    for (int index = 0; index < 7; index++)
    {
        cout << "\n" << endl;
    }
}


